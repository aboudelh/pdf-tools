from PyPDF2 import PdfFileMerger
import sys
from tkinter import Tk
from tkinter import filedialog
from tkinter import messagebox
import os

Tk().withdraw()

pdfs = []
file_types = [
    ("PDF Files", "*.pdf"),
]

cont= True
while cont:
    filename = ''
    filename =  filedialog.askopenfilename(initialdir = "/", title = "Select file Image to Convert", filetypes = file_types)
    if filename == '':
        cont = False
    else:
        pdfs.append(filename)

folder_selected = ''
folder_selected = filedialog.askdirectory(initialdir = "/", title = "Select output directory")
if folder_selected == '':
    exit()

merger = PdfFileMerger()

for pdf in pdfs:
    merger.append(pdf)

out = os.path.join(folder_selected, "merged_pdf.pdf")
merger.write(out)
merger.close()

msgString = "Following Files:\n"
for pdf in pdfs:
    msgString += pdf 
    msgString += '\n'

msgString += "were merged into:\n"+ out
messagebox.showinfo("Done Merging PDFs", msgString)