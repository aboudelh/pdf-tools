from PyPDF2 import PdfFileWriter, PdfFileReader
import sys
from tkinter import Tk
from tkinter import filedialog
from tkinter import messagebox
import os

Tk().withdraw()
if not len(sys.argv) > 1:
    print("Must enter page number to split at: <run file> <page number to split at>")
    exit()
file_types = [
    ("PDF Files", "*.pdf"),
]
filename = ''
filename =  filedialog.askopenfilename(initialdir = "/", title = "Select file Image to Convert", filetypes = file_types)
if filename == '':
    exit()

folder_selected = ''
folder_selected = filedialog.askdirectory(initialdir = "/", title = "Select output directory")
if folder_selected == '':
    exit()

page_number = int(sys.argv[1])

pdf_reader = PdfFileReader(open(filename, "rb"))
try:
    assert page_number < pdf_reader.numPages
    pdf_writer1 = PdfFileWriter()
    pdf_writer2 = PdfFileWriter()

    for page in range(page_number):
        pdf_writer1.addPage(pdf_reader.getPage(page))

    for page in range(page_number,pdf_reader.getNumPages()):
        pdf_writer2.addPage(pdf_reader.getPage(page))

    out1 = os.path.join(folder_selected, os.path.basename(filename).split('.')[0] + "_part1.pdf")
    with open(out1, 'wb') as file1:
        pdf_writer1.write(file1)

    out2 = os.path.join(folder_selected, os.path.basename(filename).split('.')[0] + "_part2.pdf")
    with open(out2, 'wb') as file2:
        pdf_writer2.write(file2)

    messagebox.showinfo("Complete Splitting PDF", "This pdf:\n{}\nwas split into:\n{}\n{}".format(filename, out1, out2))

except AssertionError as e:
    print("Error: The PDF you are cutting has less pages than you want to cut!")
