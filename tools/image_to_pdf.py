from PIL import Image
from tkinter import Tk
from tkinter import filedialog
from tkinter import messagebox
import os

Tk().withdraw()
# get file to convert
file_types = [
    ("Image Files", "*.jpg;*.jpeg;*.png;*.tif"),
]
filename = ''
filename =  filedialog.askopenfilename(initialdir = "/", title = "Select file Image to Convert", filetypes = file_types)
if filename == '':
    exit()
image1 = Image.open(open(filename, 'rb'))
im1 = image1.convert('RGB')
folder_selected = ''
folder_selected = filedialog.askdirectory(initialdir = "/", title = "Select output directory")
if folder_selected == '':
    exit()
out = os.path.join(folder_selected, os.path.basename(filename).split('.')[0] + '.pdf')
im1.save(out)
messagebox.showinfo("Complete Converting Image to PDF", "This image:\n{}\nwas converted to a PDF here:\n{}".format(filename, out))
